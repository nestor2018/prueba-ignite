function ordenes(){
  return fetch("http://2ac0ff5f.ngrok.io/api/v1/orders",{ method: 'GET',
  headers:{
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    mode: 'cors',
    cache: 'default' }).then(data=>{
    return data.json()
  }).catch(error=>{
    console.log(error)
  })
}

function ordenesPreparacion(){
  return fetch("http://2ac0ff5f.ngrok.io/api/v1/orders/preparación",{ method: 'GET',
  headers:{
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    mode: 'cors',
    cache: 'default' }).then(data=>{
    return data.json()
  }).catch(error=>{
    console.log(error)
  })
}

function ordenesTerminado(){
  return fetch("http://2ac0ff5f.ngrok.io/api/v1/orders/terminado",{ method: 'GET',
  headers:{
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    mode: 'cors',
    cache: 'default' }).then(data=>{
    return data.json()
  }).catch(error=>{
    console.log(error)
  })
}

function ordenesAnulado(){
  return fetch("http://2ac0ff5f.ngrok.io/api/v1/orders/anulado",{ method: 'GET',
  headers:{
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    mode: 'cors',
    cache: 'default' }).then(data=>{
    return data.json()
  }).catch(error=>{
    console.log(error)
  })
}

export {ordenes, ordenesPreparacion, ordenesTerminado, ordenesAnulado};
