import React, { Component, Fragment } from 'react';
import logo from './logo.svg';
import './App.sass';
import {
 BrowserRouter,
 Route
} from 'react-router-dom'


import ContainerBtn from './home/components/botones/container-botones'
import Home from './home/containers/home'
import Anulado from './home/containers/anulado'
import Preparacion from './home/containers/preparacion'
import Terminado from './home/containers/terminado'


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Fragment>
          <ContainerBtn />
          <Route exact path='/' component={Home}/>
          <Route exact path='/anulado' component={Anulado} />
          <Route exact path='/preparacion' component={Preparacion} />
          <Route exact path='/terminado' component={Terminado} />
        </Fragment>
      </BrowserRouter>
    );
  }
}

export default App;
