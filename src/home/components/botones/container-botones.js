import React, { Component } from 'react'
import { NavLink } from 'react-router-dom';

import Boton from './boton'

class ContainerBtn extends Component{
  render(){
    return(
      <div className="row justify-content-around mt-2 mb-5">
        <NavLink exact to="/" >
          <Boton title="Todos"/>
        </NavLink>
        <NavLink to="/">
          <Boton title="Nuevos" />
        </NavLink>
        <NavLink to="/preparacion">
          <Boton title="En preparación"/>
        </NavLink>
        <NavLink to="/terminado">
          <Boton title="Terminados"/>
        </NavLink>
        <NavLink to="/anulado">
          <Boton title="Anulados"/>
        </NavLink>
      </div>
    )
  }
}

export default ContainerBtn
