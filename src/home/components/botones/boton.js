import React from 'react'

function Btn(props){
  return(
    <button className="btn btn-outline-dark col-md-2">{props.title}</button>
  )
}

export default Btn
