import React from 'react'

function BodyTable(props){
  return(
    <tr>
      <th scope="row">{props.id}</th>
      <td>{props.branch}</td>
      <td>{props.created_at}</td>
      <td className="badge badge-success">{props.start_preparation}</td>
      <td className="badge badge-danger">{props.delivery_time}</td>
      <td>{props.client_id}</td>
      <td>{props.delivery_method}</td>
      <td>{props.cash_payment}</td>
      <td>{props.subtotal}</td>
      <td>{props.taxes}</td>
      <td>{props.total}</td>
      <td>{props.status}</td>
      <td>{props.updated_at}</td>
    </tr>
  )
}

export default BodyTable
