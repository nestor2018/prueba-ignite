import React, { Component } from 'react'


import BodyTable from '../bodyTable'


class Ordenes extends Component{
  render(){
    const order = this.props.orders
    console.log(this.props.data)
    return(
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Código de pedido</th>
            <th scope="col">Fecha</th>
            <th scope="col">Hora de preparación</th>
            <th scope="col">Hora de recogida</th>
            <th scope="col">Cliente</th>
            <th scope="col">Modo de entrega</th>
            <th scope="col">Metodo de pago</th>
            <th scope="col">Subtotal</th>
            <th scope="col">Impuestos</th>
            <th scope="col">Total</th>
            <th scope="col">Estado</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody>
          {
            order.map((orden,index) => {
              return <BodyTable {...orden} key={index}/>
            })
          }

        </tbody>
      </table>
    )
  }
}

export default Ordenes
