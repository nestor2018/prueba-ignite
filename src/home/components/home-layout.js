import React from 'react'



function HomeLayout(props){
  return(
    <div className="container">
      {props.children}
    </div>
  )
}

export default HomeLayout
