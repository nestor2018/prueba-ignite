import React, { Component } from 'react'

import HomeLayout from '../components/home-layout'
import Ordenes from '../components/ordenes/ordenes'
import { ordenesAnulado } from '../../request/ordenes'

class Anulado extends Component {
  constructor(props){
    super(props)
    this.state= {
      orders: []
    }
    this.cargarOrdenes()
  }

  cargarOrdenes(){
      ordenesAnulado().then(jsonR=>{
      console.log(jsonR)
      this.setState({
        orders: jsonR
      })
    })
  }
  render(){
    return(
      <HomeLayout>
        <Ordenes orders={this.orders}/>
      </HomeLayout>
    )
  }
}

export default Anulado
