import React, { Component } from 'react'

import HomeLayout from '../components/home-layout'
import Ordenes from '../components/ordenes/ordenes'
import { ordenesPreparacion } from '../../request/ordenes'

class Preparacion extends Component {
  constructor(props){
    super(props)
    this.state= {
      orders: []
    }
    this.cargarOrdenes()
  }

  cargarOrdenes(){
    ordenesPreparacion().then(jsonR=>{
      console.log(jsonR)
      this.setState({
        orders: jsonR
      })
    })
  }
  render(){
    return(
      <HomeLayout>
        <Ordenes orders={this.orders}/>
      </HomeLayout>
    )
  }
}

export default Preparacion
