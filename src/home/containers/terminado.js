import React, { Component } from 'react'

import HomeLayout from '../components/home-layout'
import Ordenes from '../components/ordenes/ordenes'
import { ordenesTerminado } from '../../request/ordenes'

class Terminado extends Component {
  constructor(props){
    super(props)
    this.state= {
      orders: []
    }
    this.cargarOrdenes()
  }

  cargarOrdenes(){
    ordenesTerminado().then(jsonR=>{
      console.log(jsonR)
      this.setState({
        orders: jsonR
      })
    })
  }
  render(){
    return(
      <HomeLayout>
        <Ordenes orders={this.orders}/>
      </HomeLayout>
    )
  }
}

export default Terminado
